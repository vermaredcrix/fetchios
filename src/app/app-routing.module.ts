import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderSummaryPage } from './checkout/order-summary/order-summary.page';
import { AgentDeliveriesPage } from './home/agent-deliveries/agent-deliveries.page';



const routes: Routes = [
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule', },
  // { path: 'agent-deliveries', component: AgentDeliveriesPage},
 
  { path: 'order-summary/:id', component: OrderSummaryPage },
  {
    path: 'welcome',
    loadChildren: () => import('./welcome/welcome.module').then( m => m.WelcomePageModule)
  },
  // {
  //   path: 'vendor-login',
  //   loadChildren: () => import('./vendor/vendor-login/vendor-login.module').then( m => m.VendorLoginPageModule)
  // },
  {
    path: 'vendor-add-products',
    loadChildren: () => import('./vendor/vendor-add-products/vendor-add-products.module').then( m => m.VendorAddProductsPageModule)
  },
  {
    path: 'ionic',
    loadChildren: () => import('./vendor/ionic/ionic.module').then( m => m.IonicPageModule)
  },
  {
    path: 'vendor-edit-order/:id',
    loadChildren: () => import('./vendor/vendor-edit-order/vendor-edit-order.module').then( m => m.VendorEditOrderPageModule)
  },
  {
    path: 'vendor-coupon-list',
    loadChildren: () => import('./vendor/vendor-coupon-list/vendor-coupon-list.module').then( m => m.VendorCouponListPageModule)
  },
  {
    path: 'driver-account-create',
    loadChildren: () => import('./vendor/driver-account-create/driver-account-create.module').then( m => m.DriverAccountCreatePageModule)
  },
  {
    path: 'store',
    loadChildren: () => import('./vendor/settings/store/store.module').then( m => m.StorePageModule)
  },
  {
    path: 'location',
    loadChildren: () => import('./vendor/settings/location/location.module').then( m => m.LocationPageModule)
  },
  {
    path: 'payment',
    loadChildren: () => import('./vendor/settings/payment/payment.module').then( m => m.PaymentPageModule)
  },
  {
    path: 'shipping',
    loadChildren: () => import('./vendor/settings/shipping/shipping.module').then( m => m.ShippingPageModule)
  },
  {
    path: 'seo',
    loadChildren: () => import('./vendor/settings/seo/seo.module').then( m => m.SeoPageModule)
  },
  {
    path: 'store-policies',
    loadChildren: () => import('./vendor/settings/store-policies/store-policies.module').then( m => m.StorePoliciesPageModule)
  },
  {
    path: 'customer-support',
    loadChildren: () => import('./vendor/settings/customer-support/customer-support.module').then( m => m.CustomerSupportPageModule)
  },
  {
    path: 'store-hours',
    loadChildren: () => import('./vendor/settings/store-hours/store-hours.module').then( m => m.StoreHoursPageModule)
  },
  {
    path: 'customer-details',
    loadChildren: () => import('./vendor/customer-detail/customer-detail.module').then( m => m.CustomerDetailPageModule)
  },
  {
    path: 'order-payments',
    loadChildren: () => import('./vendor/order-payments/order-payments.module').then( m => m.OrderPaymentsPageModule)
  },
  {
    path: 'reports',
    loadChildren: () => import('./vendor/reports/reports.module').then( m => m.ReportsPageModule)
  },
  {
    path: 'subscriptions',
    loadChildren: () => import('./vendor/subscriptions/subscriptions.module').then( m => m.SubscriptionsPageModule)
  },
  // {
  //   path: 'vendor-coupon-add',
  //   loadChildren: () => import('./vendor/vendor-coupon-add/vendor-coupon-add.module').then( m => m.VendorCouponAddPageModule)
  // },

  // {
  //   path: 'agent-deliveries',
  //   loadChildren: () => import('./home/agent-deliveries/agent-deliveries.module').then( m => m.AgentDeliveriesPageModule)
  // },
  //{ path: 'edit-address', loadChildren: './account/edit-address/edit-address.module#EditAddressPageModule' },
  //{ path: 'map', loadChildren: './account/map/map.module#MapPageModule' },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
