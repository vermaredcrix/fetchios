import { MapPage } from './../map/map.page';
import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../api.service';
import { Settings } from './../../data/settings';
import { NavigationExtras } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { OrderInfoPage } from './order_info/orders.page';
import { Storage } from '@ionic/storage';


@Component({
    selector: 'app-orders',
    templateUrl: './orders.page.html',
    styleUrls: ['./orders.page.scss'],
})
export class OrdersPage implements OnInit {
    filter: any = {};
    orders: any;
    hasMoreItems: boolean = true;
    orders_2:any;
    orders_1:any;
    user_id:any;

    constructor(public api: ApiService,
        private storage: Storage,
        public modalController: ModalController,public settings: Settings, public router: Router, public loadingController: LoadingController, public navCtrl: NavController, public route: ActivatedRoute) {
        this.filter.page = 1;
       

        console.log(this.settings.customer);
        console.log(this.settings.customer.id);
        
    }
    ngOnInit() {

        this.user_id = localStorage.getItem('user_id');

        this.storage.get('user_id').then((val) => {
            this.user_id = val;
        });

        this.filter.customer = 4;


        if(this.settings.customer)
        this.getOrders();
        this.getOrdLocation();


    }

   async getOrdLocation(){
       console.log('SETTINGS'+JSON.stringify(this.settings.customer));
       console.log('SETTINGS'+this.filter);
       

        await this.api.WCV2getAgentDeliveries('customerorderlocation?customer_id='+ this.user_id).subscribe(res => {
            console.log('======this.user_id==============',  res['response']);
            this.orders_2 = res['response'];
     
        }, err => {
            console.log(err);

        });
    }

    async getOrders() {

        console.log('----------------------->>>>>'+JSON.stringify(this.filter));

        
        
        await this.api.postFlutterItem('orders', this.filter).subscribe(res => {
            this.orders = res;
            // this.orders.sort((a, b) => {
            //     // if (a.status < 'at-location') return -1;
            //     // else if (a.status > 'at-location') return 1;
            //     // else return 0;
            //     var keyA ='at-location',
            //     keyB = 'at-location';
            //   // Compare the 2 dates
            //   if (keyA < keyB) return -1;
            //   if (keyA > keyB) return 1;
            //   return 0;

            //   });

            const sorted = this.orders.sort((t1, t2) => {
                let ss = 'at-location';
                const name1 = t1.status.toLowerCase();
                const name2 = ss.toLowerCase();
                if (name1 > name2) { return 1; }
                if (name1 < name2) { return -1; }
                return 0;
              });

              console.log('sorted'+JSON.stringify(sorted));
            

            console.log('SORETT'+this.orders);
            
        }, err => {
            console.log(err);
        });
    }

    sortJson(element, prop, propType, asc) {
        switch (propType) {
          case "int":
            element = element.sort(function (a, b) {
              if (asc) {
                return (parseInt(a[prop]) > parseInt(b[prop])) ? 1 : ((parseInt(a[prop]) < parseInt(b[prop])) ? -1 : 0);
              } else {
                return (parseInt(b[prop]) > parseInt(a[prop])) ? 1 : ((parseInt(b[prop]) < parseInt(a[prop])) ? -1 : 0);
              }
            });
            break;
          default:
            element = element.sort(function (a, b) {
              if (asc) {
                return (a[prop].toLowerCase() > b[prop].toLowerCase()) ? 1 : ((a[prop].toLowerCase() < b[prop].toLowerCase()) ? -1 : 0);
              } else {
                return (b[prop].toLowerCase() > a[prop].toLowerCase()) ? 1 : ((b[prop].toLowerCase() < a[prop].toLowerCase()) ? -1 : 0);
              }
            });
        }
      }

    async loadData(event) {
        this.filter.page = this.filter.page + 1;
        await this.api.postFlutterItem('orders', this.filter).subscribe(res => {
            this.orders.push.apply(this.orders, res);
            event.target.complete();
            if (!res) this.hasMoreItems = false;
        }, err => {
            event.target.complete();
        });
        console.log('Done');
    }

    getDetail(order) {
        let navigationExtras: NavigationExtras = {
            queryParams: {
                order: order
            }
        };
        this.navCtrl.navigateForward('/tabs/account/orders/order/' + order.id, navigationExtras);
    }

    async get_directions(order){
        // console.log(arr);
        
        const modal = await this.modalController.create({
            component: OrderInfoPage,
            componentProps: {
                order: order
            }
        });
        return await modal.present();
    }

    async getOrdInfo(id){

  
        console.log(id);
        

        let ax = [];
        ax = this.orders_2;
    
        let vv = [];
        vv.push(ax);

        console.log('??????'+JSON.stringify(vv));
        // console.log('??????'+JSON.stringify(this.orders));

        var resultObject = this.search(125470, vv[0]);

        console.log(resultObject);
        
    }


   search(nameKey, myArray){
       console.log(myArray);
        for (var i=0; i < myArray[i].length; i++) {
            if (myArray[i].order_id === nameKey) {
                return myArray[i];
            }
        }
    }
    



}