import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import {
  LoadingController,
  NavController,
  NavParams,
  Platform,
} from "@ionic/angular";
import { ActivatedRoute, Router } from "@angular/router";
import { ApiService } from "../../../api.service";
import { Settings } from "./../../../data/settings";
import { NavigationExtras } from "@angular/router";
import { ModalController } from "@ionic/angular";

declare var google;
@Component({
  selector: "app-order_info",
  templateUrl: "./orders.page.html",
  styleUrls: ["./orders.page.scss"],
})
export class OrderInfoPage implements OnInit {
  filter: any = {};
  orders: any;
  hasMoreItems: boolean = true;
  interval :any;
  @ViewChild("map", { static: false }) mapElement: ElementRef;

  map: any;
  start = "chicago, il";
  end = "chicago, il";
  directionsService = new google.maps.DirectionsService();
  directionsDisplay = new google.maps.DirectionsRenderer({
    suppressMarkers: true,
  });
  public user_id = localStorage.getItem("user_id");
  public current_order;
  public driverLat = 0;
  public driverLng = 0;
  public driver_origin_lat = 0;
  public driver_origin_lng = 0;


  icons = {
    start: new google.maps.MarkerImage(
      // URL
      "../../../../assets/image/delivery_boy.png",
      // (width,height)
      new google.maps.Size(44, 32),
      // The origin point (x,y)
      new google.maps.Point(0, 0),
      // The anchor point (x,y)
      new google.maps.Point(22, 32)
    ),
    end: new google.maps.MarkerImage(
      // URL
      "../../../../assets/image/delivery_boy.png",
      // (width,height)
      new google.maps.Size(44, 32),
      // The origin point (x,y)
      new google.maps.Point(0, 0),
      // The anchor point (x,y)
      new google.maps.Point(22, 32)
    ),
  };
  constructor(
    public api: ApiService,
    public modalController: ModalController,
    public settings: Settings,
    public router: Router,
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public route: ActivatedRoute,
    public params: NavParams,
    public platform: Platform
  ) {
    this.filter.page = 1;
    this.filter.customer = this.settings.customer.id;
    this.platform.ready().then(() => {
      this.initMap();

      this.interval = setInterval(() => { 
          this.initMap(); 
        
      }, 9000);

    });
  }
  ngOnInit() {
    console.log("navparams", this.params.get("order"));
    this.current_order = this.params.get("order");
    if (this.settings.customer) this.getOrdLocation();
  }
  closeDialog() {
    this.modalController.dismiss();
  }
  ionViewDidLoad() {}
  async initMap() {

    console.log('SETTINGS====='+this.settings.customer);
    
    this.map = new google.maps.Map(document.querySelector("#map"), {
      zoom: 7,
      center: { lat: this.driverLat, lng: this.driverLng },
    });

    this.directionsDisplay.setMap(this.map);
    this.directionsDisplay.setPanel(document.getElementById("directionsPanel"));
    await this.getOrdLocation();
  }

  calculateAndDisplayRoute(start, end) {
    this.directionsService.route(
      {
        origin: start,
        destination: end,
        travelMode: "DRIVING",
      },
      (response, status) => {
        if (status === "OK") {
          var leg = response.routes[0].legs[0];
          this.makeMarker(
            leg.start_location,
            this.icons.start,
            "title",
            this.map
          );
          this.makeMarker(leg.end_location, this.icons.end, "title", this.map);
          this.directionsDisplay.setDirections(response);
        } else {
          window.alert("Directions request failed due to " + status);
        }
      }
    );
    this.map.setCenter(start);
  }
  makeMarker(position, icon, title, map) {
    new google.maps.Marker({
      position: position,
      map: map,
      icon: icon,
      title: title,
    });
  }

  async getOrdLocation() {
    console.log("SETTINGS" + JSON.stringify(this.settings.customer));
    console.log("SETTINGS" + this.filter);

    await this.api
      .WCV2getAgentDeliveries(
        "customerorderlocation?customer_id=" + this.user_id
      )
      .subscribe(
        (res) => {
          // console.log("======this.user_id==============", res["response"]);
          // console.log(res["response"]);

          for (let i in res["response"]) {
            // console.log("item", item);
            var item = res["response"][i];
            if (item["order_id"] == this.current_order) {
                console.log("item", item);
                this.driver_origin_lat = 0;
                this.driver_origin_lng = 0;

              this.driver_origin_lat = parseFloat(item["origin_lat"]);
              this.driver_origin_lng = parseFloat(item["origin_lng"]);
              var pos = {
                lat: this.driver_origin_lat,
                lng: this.driver_origin_lng,
              };

              console.log(pos);

              this.driverLat = 0;
              this.driverLng = 0;

            this.driverLat = parseFloat(item["current_lat"]);
            this.driverLng =  parseFloat(item["current_lng"]);

              
              var end = {
                lat: this.driverLat,
                lng: this.driverLng,
              };

              console.log(end);
              

              
              //   var infoWindow;
              // infoWindow.setPosition(pos);
              //  infoWindow.setContent("Location found.");
              //   infoWindow.open(this.map);
              // var end = { lat: 21.2116848, lng: 72.8733821 };
              this.calculateAndDisplayRoute(pos, end);
              this.map.setCenter(pos);
            }
          }
        },
        (err) => {
          console.log(err);
        }
      );
  }
  async getOrders() {
    console.log("filter", this.filter);

    await this.api.postFlutterItem("orders", this.filter).subscribe(
      (res) => {
        this.orders = res;
        console.log(this.orders);
      },
      (err) => {
        console.log(err);
      }
    );
  }
  async loadData(event) {
    this.filter.page = this.filter.page + 1;
    await this.api.postFlutterItem("orders", this.filter).subscribe(
      (res) => {
        this.orders.push.apply(this.orders, res);
        event.target.complete();
        if (!res) this.hasMoreItems = false;
      },
      (err) => {
        event.target.complete();
      }
    );
    console.log("Done");
  }

  async get_directions() {
    let arr = [];
    // Object.keys(pass).map(function (key) {
    //     arr.push({ [key]: pass[key] })
    //     return arr;
    // });
  }
  getDetail(order) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        order: order,
      },
    };
    this.navCtrl.navigateForward(
      "/tabs/account/orders/order/" + order.id,
      navigationExtras
    );
  }
}