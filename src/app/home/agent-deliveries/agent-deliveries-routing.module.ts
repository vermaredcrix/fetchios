import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AgentDeliveriesPage } from './agent-deliveries.page';

const routes: Routes = [
  {
    path: '',
    component: AgentDeliveriesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AgentDeliveriesPageRoutingModule {}
