import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import {
  LoadingController,
  NavController,
  AlertController,
} from "@ionic/angular";
import { ActivatedRoute, Router } from "@angular/router";
import { ApiService } from "../api.service";
import { Data } from "../data";
import { Settings } from "../data/settings";
import { Product } from "../data/product";
import { OneSignal } from "@ionic-native/onesignal/ngx";
import { NativeStorage } from "@ionic-native/native-storage/ngx";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { Platform } from "@ionic/angular";
import { Config } from "../config";
import { TranslateService } from "@ngx-translate/core";
import { Vendor } from "../data/vendor";
import { Storage } from "@ionic/storage";
import { ModalController } from "@ionic/angular";
import { AgentDeliveriesPage } from "./agent-deliveries/agent-deliveries.page";
import { Location } from "@angular/common";
import {
  LaunchNavigator,
  LaunchNavigatorOptions,
} from "@ionic-native/launch-navigator/ngx";
import {
  NativeGeocoder,
  NativeGeocoderResult,
  NativeGeocoderOptions,
} from "@ionic-native/native-geocoder/ngx";
import { Observable, of, throwError } from "rxjs";
import { Geolocation } from "@ionic-native/geolocation/ngx";
import { NgZone } from "@angular/core";
import { ApplicationRef } from "@angular/core";
// import {ViewController} from '@ionic/angular';
import * as moment from "moment";
import { NavigationExtras } from "@angular/router";
import { CallNumber } from "@ionic-native/call-number";

// import {
//   BackgroundGeolocation,
//   BackgroundGeolocationConfig,
//   BackgroundGeolocationResponse,
//   BackgroundGeolocationEvents,
// } from "@ionic-native/background-geolocation";
import { ToastController } from "@ionic/angular";
import { BackgroundMode } from '@ionic-native/background-mode/ngx';

declare var google;

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"],
})
export class HomePage {
  tempProducts: any = [];
  filter: any = {};
  hasMoreItems: boolean = true;
  cart: any;
  vendors: any = [];
  vendors_s: any = [];
  vendorsData: any = [];
  SelectedVendor;
  screenWidth: any = 300;
  showVendor = false;
  slideOpts = {
    effect: "flip",
    autoplay: true,
    parallax: true,
    loop: true,
    lazy: true,
  };
  byVendor = false;
  products: any;
  deliveryAgentRes: any;
  isDeliveryAgent: any;
  deliveryAgentID: any;
  vendorID: any;
  isVendorLogin: any;
  orderDetails_: boolean = false;
  SelectedUpdatedStatus = "";
  HttpFailurForDeliveryBoyorderDetails_: boolean = false;
  availabilityVen: boolean = true;
  catchDriverAvl: any;
  user_id: any;
  isVendorLoginO: any;
  hasSearch = false;
  updateStatuses = [
    { statusToUpdate: "on-the-way", val: 'wc-on-the-way' },
    { statusToUpdate: "at-location" , val:  'wc-at-location'},
    { statusToUpdate: "complete",  val:  'wc-completed'},
  ];
  currentLat: any;
  currentLng: any;
  userLoggedIn: any;
  userLogged_In:any;
  newArray = [];
  dataaa;
  deliveryLocationList = [
    { location: "Current location", val: "1" },
    { location: "Home Address", val: "2" },
    { location: "Find address", val: "3" },
  ];

  deliveryLocationList_NoUserLogin = [
    { location: "Current location", val: "1" },
    { location: "Find address", val: "3" },
  ];

  SelectDeliveryLocation;
  customerDeliveryAddString: any;
  DelLat: any;
  DelLng: any;
  usersLocation = {};
  loggedIn: any;
  SelectDeliveryLocationName: any;
  SelectDeliveryUnavailable: boolean = false;
  searchPlaces: boolean = false;
  searchPlacesX: boolean = false;
  searchPlacesList: boolean = false;
  autocompleteItems;
  autocomplete;
  errorTxt = "";

  latitude: number = 0;
  longitude: number = 0;
  geo: any;
  VendorNameDis = "";
  NoMetrics = true;
  filter_: any = {};

  BannerOption = {
    initialSlide: 0,
    slidesPerView: 2.3,
    loop: true,
    centeredSlides: false,
    autoplay: false,
    speed: 500,
    spaceBetween: 7,
  };

  SearchOption = {
    initialSlide: 0,
    slidesPerView: 3.5,
    loop: false,
    centeredSlides: false,
    autoplay: false,
    speed: 500,
    spaceBetween: -20,
  };

  TrendOption = {
    initialSlide: 0,
    slidesPerView: 1.4,
    loop: true,
    centeredSlides: false,
    autoplay: false,
    speed: 800,
    spaceBetween: -9,
  };

  MiddleBannerOption = {
    initialSlide: 0,
    slidesPerView: 1.3,
    loop: false,
    centeredSlides: false,
    autoplay: true,
    speed: 800,
    spaceBetween: 7,
  };

  public E_items: any = [];
  itemExpanded: boolean = false;
  service = new google.maps.places.AutocompleteService();
  previous_login: any;
  OrderMetr: any;
  grossSale: any = 0;
  earing: any = 0;
  items_purchased: any = 0;
  number_of_orders: any = 0;
  order_id_: any;
  date_start: any;
  end_start: any;
  VendorOrders_: any;
  objectKeys = Object.keys;
  filterSale_: any;
  order_status_ven: any;
  Messg = "";
  store_name: any;
  vendor_detail_: any;
  livelocation: any;
  order_ids = [];
  count=0;
  custom_dates_view = false;
  products_s = false;
  Subscript:any;
  searchInput: any = "";
  ActiveLast:any;
  membership_status = false;


  constructor(
    private config: Config,
    public vendor: Vendor,
    private location: Location,
    private storage: Storage,
    public modalController: ModalController,
    private launchNavigator: LaunchNavigator,
    private alertCtrl: AlertController,
    private nativeGeocoder: NativeGeocoder,
    private geolocation: Geolocation,
    private zone: NgZone,
    private app: ApplicationRef,
    public toastController: ToastController,
    // private backgroundGeolocation: BackgroundGeolocation,
    private backgroundMode: BackgroundMode,
    public api: ApiService,
    private splashScreen: SplashScreen,
    public platform: Platform,
    public translateService: TranslateService,
    public data: Data,
    public settings: Settings,
    public product: Product,
    public loadingController: LoadingController,
    public router: Router,
    public navCtrl: NavController,
    public route: ActivatedRoute,
    private oneSignal: OneSignal,
    private nativeStorage: NativeStorage
  ) {
  
  }

  ionViewDidLoad(){
    this.filter.page = 1;
    this.filter.status = "publish";
    this.screenWidth = this.platform.width();
    this.renderUser_();
  }

  renderUser_() {
    this.userLoggedIn = true;
    this.userLogged_In = false;
    this.autocompleteItems = [];
    this.autocomplete = {
      query: "",
    };

    this.E_items = [{ expanded: false }];

    this.deliveryAgentID = localStorage.getItem("deliveryAgentID");
    this.isDeliveryAgent = localStorage.getItem("isDeliveryAgent");
    this.loggedIn = localStorage.getItem("logginInVal");
    this.user_id = localStorage.getItem("user_id");

    //    extra cover for iOS

    this.vendorID = localStorage.getItem("vendorID");
    this.isVendorLogin = localStorage.getItem("isVendorLogin");

    this.storage.get("isVendorLogin").then((val) => {
      this.isVendorLogin = val;
    });

    this.storage.get("vendorID").then((val) => {
      this.vendorID = val;
    });

    //

    this.storage.get("deliveryAgentID").then((val) => {
      this.deliveryAgentID = val;
    });

    this.storage.get("user_id").then((val) => {
      this.user_id = val;
    });

    this.storage.get("isDeliveryAgent").then((val) => {
      this.isDeliveryAgent = val;
    });

    this.storage.get("logginInVal").then((val) => {
      this.loggedIn = val;
    });

    // at login time.

    if (this.loggedIn == "true") {
      this.userLoggedIn = true;
      this.isVendorLoginO = false;

      this.storage.get("vendorID").then((val) => {
        this.vendorID = val;
      });

      if (this.isDeliveryAgent == "true") {
        this.isVendorLoginO = false;
        this.searchPlacesX = false;
        this.searchPlacesList = false;
        console.log(" ==> AGENT");
        this.getDeliveryDetailsForAgent();
      }

      if (this.isVendorLogin == "true") {
        this.searchPlacesX = false;
        this.searchPlacesList = false;
        this.isVendorLoginO = true;
        this.getOrderMetrics();
        this.getOrders__();
        // this.getWCFMVendors();
        console.log(" ==> VENDOR");
        this.data.blocks = [];
        // this.getBlocks();
        this.getVendors_sub();
        // this.getDeliveryDetailsForAgent();
        // this.data.blocks = []
        // this.getBlocks();
        // this.products = []
      }

      if (this.isDeliveryAgent == "true" || this.isVendorLogin == "true") {
        this.userLoggedIn = false;
        console.log("No User logged in .. ", this.isVendorLogin);
      } else {
        // this.userLoggedIn = true;

        // current Location
        this.geolocation
          .getCurrentPosition()
          .then((resp) => {
            this.currentLat = "";
            this.currentLng = "";
            this.currentLat = resp.coords.latitude;
            this.currentLng = resp.coords.longitude;
          })
          .catch((error) => {
            console.log("Error getting location", error);
          });

        let watch = this.geolocation.watchPosition();
        watch.subscribe((data) => {
          this.currentLat = "";
          this.currentLng = "";
          this.currentLat = data.coords.latitude;
          this.currentLng = data.coords.longitude;
        });

        this.usersLocation = {};

        this.usersLocation = {
          lat: this.currentLat,
          lng: this.currentLng,
        };

        console.log("== > NO AGENT" + this.currentLat);
        this.getBlocks();
        this.getWCFMVendors();
      }

      // console.log('== > User logged in.');
    } else {
      if (this.loggedIn == "true"){
        this.userLogged_In = false;
      }
      else if(this.loggedIn != "true") {
        this.userLoggedIn = false;
        this.userLogged_In = true;
      };
     
      // current Location
      this.geolocation
        .getCurrentPosition()
        .then((resp) => {
          this.currentLat = "";
          this.currentLng = "";
          this.currentLat = resp.coords.latitude;
          this.currentLng = resp.coords.longitude;
        })
        .catch((error) => {
          console.log("Error getting location", error);
        });

      let watch = this.geolocation.watchPosition();
      watch.subscribe((data) => {
        this.currentLat = "";
        this.currentLng = "";
        this.currentLat = data.coords.latitude;
        this.currentLng = data.coords.longitude;
      });

      this.usersLocation = {};

      this.usersLocation = {
        lat: this.currentLat,
        lng: this.currentLng,
      };

      if (this.isDeliveryAgent == "true") {
        this.getDeliveryDetailsForAgent();
      } else {
        this.getWCFMVendors();
      }
    }
  }

  ngOnInit() {
    // this.userLoggedIn = true;

    this.nativeStorage.getItem("blocks").then(
      (data) => {
        this.data.blocks = data.blocks;
        this.data.categories = data.categories;
        this.data.mainCategories = this.data.categories.filter(
          (item) => item.parent == 0
        );
        this.settings.pages = this.data.blocks.pages;
        this.settings.settings = this.data.blocks.settings;
        this.settings.dimensions = this.data.blocks.dimensions;
        this.settings.currency = this.data.blocks.settings.currency;

        if (this.data.blocks.languages)
          this.settings.languages = Object.keys(this.data.blocks.languages).map(
            (i) => this.data.blocks.languages[i]
          );
        this.settings.currencies = this.data.blocks.currencies;
        this.settings.calc(this.platform.width());
        if (this.settings.colWidthLatest == 4) this.filter.per_page = 15;
        //this.settings.theme = this.data.blocks.theme;
        this.splashScreen.hide();
      },
      (error) => console.error(error)
    );

    this.nativeStorage.getItem("settings").then(
      (data) => {
        if (data.lang) {
          this.config.lang = data.lang;
          this.translateService.setDefaultLang(data.lang);
          if (data.lang == "ar") {
            document.documentElement.setAttribute("dir", "rtl");
          }
        }
      },
      (error) => console.error(error)
    );
  }

  ionViewWillEnter() {
    this.renderUser_();

    // this.userLoggedIn = true;

    // this.searchPlaces = false;
    // this.orderDetails_ = false;

    // this.HttpFailurForDeliveryBoyorderDetails_ = false;
    // // this.isDeliveryAgent = 'false;

    // // console.log(localStorage.getItem('logginIn'));

    // if (localStorage.getItem('logginIn') == 'true') {
    //     // localStorage.removeItem('logginIn');
    //     this.userLoggedIn = true;
    //     // console.log('????');

    //     this.isVendorLoginO = false;

    //     this.storage.get('deliveryAgentID').then((val) => {
    //         this.deliveryAgentID = val;
    //     });

    //     this.deliveryAgentID = localStorage.getItem('deliveryAgentID');
    //     this.isDeliveryAgent = localStorage.getItem('isDeliveryAgent');
    //     this.loggedIn = localStorage.getItem('logginInVal');
    //     this.user_id = localStorage.getItem('user_id');

    //     //

    //     this.vendorID = localStorage.getItem('vendorID');
    //     this.isVendorLogin = localStorage.getItem('isVendorLogin');

    //     this.storage.get('isVendorLogin').then((val) => {
    //         this.isVendorLogin = val;
    //     });

    //     this.storage.get('vendorID').then((val) => {
    //         this.vendorID = val;
    //     });

    //     this.storage.get('user_id').then((val) => {
    //         this.user_id = val;
    //     });
    //     this.storage.get('deliveryAgentID').then((val) => {
    //         this.deliveryAgentID = val;
    //     });
    //     this.storage.get('isDeliveryAgent').then((val) => {
    //         this.isDeliveryAgent = val;
    //     });

    //     this.storage.get('logginInVal').then((val) => {
    //         this.loggedIn = val;
    //     });

    //     if (this.isDeliveryAgent == 'true') {
    //         this.userLoggedIn = false;
    //         this.searchPlacesX = false;
    //         this.searchPlacesList = false;
    //         console.log(' ==> AGENT');
    //         this.getDeliveryDetailsForAgent();
    //         this.data.blocks = []
    //         this.getBlocks();
    //         this.products = []
    //     }

    //     if (this.isVendorLogin == 'true') {
    //         this.userLoggedIn = false;
    //         this.isVendorLoginO = true;
    //         this.searchPlacesX = false;
    //         this.searchPlacesList = false;
    //         this.data.blocks = []
    //         this.getBlocks();

    //     }

    //     if (this.isDeliveryAgent == 'true' || this.isVendorLogin == 'true') {
    //         console.log('Not logged in USER');

    //         this.searchPlacesX = false;
    //         this.userLoggedIn = false;
    //     }

    // }
    // console.log(localStorage.getItem('logginIn'));

    //      if(localStorage.getItem('logginIn') == null) {

    //         // this.userLoggedIn = true;
    //         this.deliveryAgentRes = []
    //         this.data.blocks = []
    //         this.settings.customer = {};
    //         console.log('== > NO AGENT');
    //         this.getBlocks();
    //         // > Customer > Address > GetLatLng >
    //         // > Vendor >
    //         this.getWCFMVendors();

    //     }

    //     if (this.isDeliveryAgent == 'true' || this.isVendorLogin == 'true') {
    //         console.log('Not logged in USER');

    //         this.userLoggedIn = false;
    //     }
  }

  async getDeliveryDetailsForAgent() {
    this.errorTxt = "";
    await this.api
      .WCV2getAgentDeliveries("getMyOrders?agent_id=" + this.deliveryAgentID)
      .subscribe(
        (res) => {
 
        
          this.orderDetails_ = true;
          this.deliveryAgentRes = [];
          // this.deliveryAgentRes = res["response"];
          this.zone.run(() => this.deliveryAgentRes = res["response"]);

          console.log("============???????????==========", res);

          // console.log(res["response"]["0"].agent_availability_status);
          if (res["response"]["0"].agent_availability_status == "available") {
            this.availabilityVen = true;
            // console.log('status = true');
          }

          if (res["response"]["0"].agent_availability_status == "unavailable") {
            this.availabilityVen = false;
            // console.log('status = false');
          }

          if (res["response"]["0"].agent_availability_status == "") {
            this.availabilityVen = false;
            // console.log('status = false');
          }

          if ((res["status"] = "error")) {
            // console.log('error');

            this.errorTxt = res["msg"];
          }

          // this.availabilityVen =
          // console.log(this.deliveryAgentRes);
        },
        (err) => {
          this.orderDetails_ = false;
          this.HttpFailurForDeliveryBoyorderDetails_ = true;
          console.log("Erorr================", err);
        }
      );
  }

  selectStatus(event: any) {
    this.updateDeliveryDetailsForAgent(this.SelectedUpdatedStatus, event);

    if (this.SelectedUpdatedStatus == "on-the-way") {
      console.log("location tracking enabled");

      this.order_ids.push(parseInt(event));

      console.log(this.order_ids);

      this.livelocation = setInterval(() => {
        // this.order_ids = [];

        this.trackDriverLocation();
      }, 5000);

    }

    if (this.SelectedUpdatedStatus == "at-location") {
      console.log("Stop tracking");
      this.backgroundMode.disable();
      clearInterval(this.livelocation);
    }

    this.order_id_ = event;
    this.SelectedUpdatedStatus = "";
  }

  async updateDeliveryDetailsForAgent(status, order_id) {
    console.log(status);

    this.availabilityVen;
    // await this.api
    //   .WCV2getAgentDeliveries(
    //     "updatestatus?order_id=" + order_id + "&status=" + status
    //   )
    //   .subscribe(

      let sen_data = {
        // vendor_id : this.vendorID,
        order_id : order_id,
        status : status,
        // password : this.account_detail.value.password,
        // email: this.account_detail.value.email,
        // phone: this.account_detail.value.phone,
        // first_name: this.account_detail.value.first_name,
        // last_name: this.account_detail.value.last_name,
        // availability_status: this.account_detail.value.availability_status
      }
      // https://fetch.betaplanets.com/wp-json/mobileapi/v1/getsubscription
  
        await this.api.update_POST_parms('updatestatus', sen_data).subscribe(res => {
 
          // console.log('=====================', res);
          this.orderDetails_ = true;

          this.getDeliveryDetailsForAgent();
        },
        (err) => {
          this.HttpFailurForDeliveryBoyorderDetails_ = true;
          console.log(err);
        }
      );
  }

  async getWCFMVendors() {
    // await this.api.postItem('wcfm-vendor-list', this.filter).subscribe(res => {
    //     // console.log('VENDOR LIS', res);

    //     // this.vendorsData = res;
    //     console.log(this.vendorsData);

    //     if (this.loggedIn == 'true') {
    //         // console.log('await ..');
    //         this.commonFiltere();
    //     }
    //     else {
    //         this.commonFiltere();
    //     }

    // }, err => {
    //     console.log(err);
    // });

    await this.api
      .WCV2getAgentDeliveries("vendordetails?start=0&limit=500")
      .subscribe(
        (res) => {
          // console.log('=======VENDOR_Products_slider_menu_SCSS==============', res);
          this.vendorsData = res["response"];

          if (this.loggedIn == "true") {
            // console.log('await ..');
            this.commonFiltere();
          } else {
            this.commonFiltere();
          }
        },
        (err) => {
          this.HttpFailurForDeliveryBoyorderDetails_ = true;
          console.log(err);
        }
      );
  }

  selectVendor(o) {
    this.vendor_detail_ = [];
    this.vendor_detail_.push(o);
    console.log(this.vendor_detail_);

    console.log(this.SelectedVendor.ID);
    // this.vendor_detail_ = o;

    this.VendorNameDis = "";
    this.VendorNameDis = o.display_name;
    this.showVendor = true;
    this.byVendor = true;
    // this.getProducts(this.SelectedVendor.ID);

    // for (let i in o) {

    //     var item = o[i];
    //     // console.log("item", item);

    //     if (item.display_name == this.VendorNameDis) {
    //         console.log(item);

    //         console.log(this.vendor_detail_);

    //       };
    //     }
  }

  getProducts(id) {
    let postVen = {
      page: "1,2,3,4,5",
      status: "publish",
      vendor: id,
    };
    this.api.postFlutterItem("products", postVen).subscribe(
      (res) => {
        this.products = res;
        this.byVendor = true;
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getVendor_All_products() {
    // console.log('========>>', this.SelectedVendor);
    this.vendor.vendor = "";
    this.vendor.vendor = this.SelectedVendor;
    this.navCtrl.navigateForward("/tabs/vendor/products");
  }

  getCart() {
    this.api.postItem("cart").subscribe(
      (res) => {
        this.cart = res;
        this.data.updateCart(this.cart.cart_contents);
        this.data.cartNonce = this.cart.cart_nonce;
      },
      (err) => {
        console.log(err);
      }
    );
  }

  async getBlocks() {
    await this.getCurrentCusData();

  }
 async getBlocks2() {



    this.api.postItem("keys").subscribe(
      (res) => {

        
        this.data.blocks = res;
        console.log('==========================-----------------------------------------------------------'+JSON.stringify(this.data.blocks));

        if (this.data.blocks.user)
          this.settings.user = this.data.blocks.user.data;

        this.settings.pages = this.data.blocks.pages;
        if (this.data.blocks.user)
          this.settings.reward = this.data.blocks.user.data.points_vlaue;
        if (this.data.blocks.languages)
          this.settings.languages = Object.keys(this.data.blocks.languages).map(
            (i) => this.data.blocks.languages[i]
          );
        this.settings.currencies = this.data.blocks.currencies;
        this.settings.settings = this.data.blocks.settings;
        this.settings.dimensions = this.data.blocks.dimensions;
        this.settings.currency = this.data.blocks.settings.currency;
        if (this.data.blocks.categories) {
          this.data.categories = this.data.blocks.categories.filter(
            (item) => item.name != "Uncategorized"
          );
          this.data.mainCategories = this.data.categories.filter(
            (item) => item.parent == 0
          );
        }
        this.settings.calc(this.platform.width());
        if (this.settings.colWidthLatest == 4) this.filter.per_page = 15;
        this.splashScreen.hide();


        
        // this.getCart();
        // this.processOnsignal();
        if (this.data.blocks.user) {


          this.settings.customer = {};
          this.settings.customer.id = this.data.blocks.user.ID;
          if (
            this.data.blocks.user.allcaps.dc_vendor ||
            this.data.blocks.user.allcaps.seller ||
            this.data.blocks.user.allcaps.wcfm_vendor
          ) {
            this.settings.vendor = true;
          }
          console.log('==========================-----------------------------------------------------------'+this.data.blocks);

        }
        for (let item in this.data.blocks.blocks) {
          var filter;
          if (this.data.blocks.blocks[item].block_type == "flash_sale_block") {
            this.data.blocks.blocks[item].interval = setInterval(() => {
              var countDownDate = new Date(
                this.data.blocks.blocks[item].sale_ends
              ).getTime();
              var now = new Date().getTime();
              var distance = countDownDate - now;
              this.data.blocks.blocks[item].days = Math.floor(
                distance / (1000 * 60 * 60 * 24)
              );
              this.data.blocks.blocks[item].hours = Math.floor(
                (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
              );
              this.data.blocks.blocks[item].minutes = Math.floor(
                (distance % (1000 * 60 * 60)) / (1000 * 60)
              );
              this.data.blocks.blocks[item].seconds = Math.floor(
                (distance % (1000 * 60)) / 1000
              );
              if (distance < 0) {
                clearInterval(this.data.blocks.blocks[item].interval);
                this.data.blocks.blocks[item].hide = true;
              }
            }, 1000);
          }
        }
        if (this.data.blocks.settings.show_latest) {
          this.data.products = this.data.blocks.recentProducts;
        }
        if (this.data.blocks.user) {
          this.api.postItem("get_wishlist").subscribe(
            (res) => {
              for (let item in res) {
                this.settings.wishlist[res[item].id] = res[item].id;
              }
            },
            (err) => {
              console.log(err);
            }
          );
        }

        this.nativeStorage
          .setItem("blocks", {
            blocks: this.data.blocks,
            categories: this.data.categories,
          })
          .then(
            () => console.log("Stored item!"),
            (error) => console.error("Error storing item", error)
          );


        if (this.data.blocks.settings.switchAddons) {
          this.api.getAddonsList("product-add-ons").subscribe((res) => {
            this.settings.addons = res;
          });
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }
  goto(item) {
    if (item.description == "category")
      this.navCtrl.navigateForward("/tabs/home/products/" + item.url);
    else if (item.description == "product")
      this.navCtrl.navigateForward("/tabs/home/product/" + item.url);
    else if (item.description == "post")
      this.navCtrl.navigateForward("/tabs/home/post/" + item.url);
  }
  getProduct(item, i) {
    console.log(i);
    this.product.product = i;
    this.product.product["vendor"] = {};

    this.product.product["vendor"]["store_name"] = item;

    console.log(this.product);
    //    let a : {};

    //    a = JSON.stringify(this.product);
    //     a["vendor"]["store_name"] =

    // this.product[0].vendor.store_name = item ;

    console.log("????" + this.store_name);

    this.navCtrl.navigateForward("/tabs/home/product/" + i.product_id);
  }

  getProductP(item) {
    console.log(item);

    this.product.product = item;
    this.navCtrl.navigateForward("/tabs/home/product/" + item.id);
  }
  getSubCategories(id) {
    const results = this.data.categories.filter(
      (item) => item.parent === parseInt(id)
    );
    return results;
  }
  getCategory(id) {
    this.navCtrl.navigateForward("/tabs/home/products/" + id);
  }
  loadData(event) {
    this.filter.page = this.filter.page + 1;
    this.api.postFlutterItem("products", this.filter).subscribe(
      (res) => {
        this.tempProducts = res;
        this.data.products.push.apply(this.data.products, this.tempProducts);
        event.target.complete();
        if (this.tempProducts.length == 0) this.hasMoreItems = false;
      },
      (err) => {
        event.target.complete();
      }
    );
  }
  processOnsignal() {
    this.oneSignal.startInit(
      this.data.blocks.settings.onesignal_app_id,
      this.data.blocks.settings.google_project_id
    );
    //this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
    this.oneSignal.handleNotificationReceived().subscribe(() => {
      //do something when notification is received
    });
    this.oneSignal.handleNotificationOpened().subscribe((result) => {
      if (result.notification.payload.additionalData.category) {
        this.navCtrl.navigateForward(
          "/tabs/home/products/" +
            result.notification.payload.additionalData.category
        );
      } else if (result.notification.payload.additionalData.product) {
        this.navCtrl.navigateForward(
          "/tabs/home/product/" +
            result.notification.payload.additionalData.product
        );
      } else if (result.notification.payload.additionalData.post) {
        this.navCtrl.navigateForward(
          "/tabs/home/post/" + result.notification.payload.additionalData.post
        );
      } else if (result.notification.payload.additionalData.order) {
        this.navCtrl.navigateForward(
          "/tabs/account/orders/order/" +
            result.notification.payload.additionalData.order
        );
      }
    });
    this.oneSignal.endInit();
  }
  doRefresh(event) {
    this.filter.page = 1;
    this.getBlocks();
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }
  getHeight(child) {
    return (child.height * this.screenWidth) / child.width;
  }

  async viewOrderDetailsDeliveryAgent(pass) {
    let arr = [];
    Object.keys(pass).map(function (key) {
      arr.push({ [key]: pass[key] });
      return arr;
    });

    console.log(arr);

    const modal = await this.modalController.create({
      component: AgentDeliveriesPage,
      componentProps: {
        pass: arr,
      },
    });
    return await modal.present();
  }

  openMapNavigator(location) {
    console.log(location);

    let redirectAddress = location._billing_address_1;
    if (location._billing_address_2) {
      redirectAddress += ", " + location._billing_address_2;
    }
    if (location._billing_city) {
      redirectAddress += ", " + location._billing_city;
    }
    if (location._billing_state) {
      redirectAddress += ", " + location._billing_state;
    }

    // redirectAddress = "46A, Sector 46, Chandigarh, 160047"
    const isApp =
      !document.URL.startsWith("http") ||
      document.URL.startsWith("http://google.com");
    // if (this.platform.is("pwa"  || !isApp) {
    // if (this.platform.is("pwa" || !isApp) {
    // console.log('http://localhost:8000/#/tabs/home');

    if (location) {
      console.log(this.platform.platforms());
      
      window.open(
        "https://www.google.com/maps/dir/?api=1&travelmode=driving&destination=" +
          redirectAddress
      );



      // const options: LaunchNavigatorOptions = {
      //   transportMode: this.launchNavigator.TRANSPORT_MODE.DRIVING,
      // };
      // this.launchNavigator.navigate(redirectAddress, options).then(
      //   (success) => console.log("Launched navigator"),
      //   (error) => console.log("Error launching navigator", error)
      // );
      
    } 
    // else {

    // }
  }

  getCurrentLocation() {
    // get pin point from address text

    if (this.settings.customer.billing.address_1) {
      this.customerDeliveryAddString = "";
      this.customerDeliveryAddString =
        this.settings.customer.shipping.address_1 +
        ", " +
        this.settings.customer.shipping.address_2 +
        ", " +
        this.settings.customer.shipping.city +
        ", " +
        this.settings.customer.shipping.country;
    }

    if (this.settings.customer.billing.address_1) {
      this.customerDeliveryAddString = "";
      this.customerDeliveryAddString =
        this.settings.customer.billing.address_1 +
        ", " +
        this.settings.customer.billing.address_2 +
        ", " +
        this.settings.customer.billing.city +
        ", " +
        this.settings.customer.billing.country;
    }

    return new Promise((resolve) => {
      // console.log('thiss???', this.customerDeliveryAddString);

      var geocoder = new google.maps.Geocoder();

      geocoder.geocode(
        { address: JSON.stringify(this.customerDeliveryAddString) },
        function (results, status) {
          console.log(results);
          if (status == google.maps.GeocoderStatus.OK) {
            this.DelLat = results[0].geometry.viewport.Ua.i;
            this.DelLng = results[0].geometry.viewport.Ya.i;
            console.log(this.DelLat);
          } else {
            results = "Unable to find address: " + status;
          }
        }
      );
    });
  }

  async updateAvailabilityStatus(ev) {
    console.log(ev.detail.checked);

    this.catchDriverAvl = "";

    if (ev.detail.checked == true) {
      this.catchDriverAvl = "available";
    }
    if (ev.detail.checked == false) {
      this.catchDriverAvl = "unavailable";
    }

    await this.api
      .WCV2getAgentDeliveries(
        "updateavailability?agent_id=" +
          this.deliveryAgentID +
          "&status=" +
          this.catchDriverAvl
      )
      .subscribe(
        (res) => {
          console.log("=====================", res);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  deliveryLocation() {
    // console.log(this.searchPlacesX);

    this.searchPlaces = false;

    if (this.SelectDeliveryLocation == 1) {
      this.searchPlacesX = false;
      this.searchPlacesList = false;
      this.commonFiltere();
    }

    if (this.SelectDeliveryLocation == 2) {
      this.searchPlacesX = false;
      this.searchPlacesList = false;

      this.commonFiltere();
    }

    if (this.SelectDeliveryLocation == 3) {
      this.searchPlacesX = true;
      // this.vendors = [];
      // this.vendors =  this.vendorsData;
      // console.log('PLACES API');
    }
  }

  // to calculate delivery radius between 2 locations

  commonFiltere() {
    this.vendors = [];
    this.vendors = this.vendorsData;

    // console.log('DEBUG =================================== 1'+JSON.stringify(this.vendors));

    let _ven = this.vendors;
    const newArray = _ven.map((o) => {
      return [o.vendor_dash_details.cus_lat, o.vendor_dash_details.cus_lng];
    });
    this.newArray = newArray;

    // console.log('DEBUG =================================== 2'+JSON.stringify(this.newArray));

    return new Promise((resolve) => {
      this.vendors = this.applyHaversine(this.vendorsData);
      this.vendors.sort((locationA, locationB) => {
        return locationA.distance - locationB.distance;
      });
      // console.log('DEBUG =======SORTED============================ 3===='+JSON.stringify(this.vendors));

      // parseInt(f.vendor_dash_details._wcfm_delivery_radius)

      // let vl value = parseFloat(value).toFixed(2);

      let Fil = this.vendors.filter((f) => parseInt(f.distance) <= parseInt(f.vendor_dash_details._wcfm_delivery_radius));
      this.vendors = "";
      this.vendors = Fil;

      this.getBlocks2();
      // this.app.tick();
      // console.log('Filtered??', JSON.stringify(this.vendors));
    });
  }

  applyHaversine(locations) {
    if (this.searchPlacesList == true) {
      this.usersLocation = {};
      this.usersLocation = {
        lat: this.latitude,
        lng: this.longitude,
      };
    }

    if (this.SelectDeliveryLocation == 1) {
      console.log(
        "this.currentLat customise=================================",
        this.currentLat
      );

      if (typeof this.currentLat == "undefined") {
        this.searchPlacesX = true;
      }
      if (this.currentLat === "") {
        this.searchPlacesX = true;
      }

      this.usersLocation = {};
      this.usersLocation = {
        lat: this.currentLat,
        lng: this.currentLng,
      };
    } else if (this.SelectDeliveryLocation == 2) {
      if (this.loggedIn == "true") {
        // console.log(this.settings.customer);

        console.log("??????=======" + JSON.stringify(this.settings.customer));
        this.usersLocation = {};
        if (this.settings.customer.cus_lat) {
          this.usersLocation = {};
          this.usersLocation = {
            lat: this.settings.customer.cus_lat,
            lng: this.settings.customer.cus_lng,
          };

          console.log(this.usersLocation);
        }
      } else {
        this.vendors = [];
        this.presentOptions();
        
      }
    } else if (this.SelectDeliveryLocation == 3) {
      this.usersLocation = {};
      this.usersLocation = {
        lat: this.latitude,
        lng: this.longitude,
      };
    } else if (this.loggedIn == "true") {
      // console.log('Select customise', this.SelectDeliveryLocation);
      // console.log('saves address converted', this.settings.customer);

      this.usersLocation = {};
      if (this.settings.customer.cus_lat) {
        this.usersLocation = {};
        this.usersLocation = {
          lat: this.settings.customer.cus_lat,
          lng: this.settings.customer.cus_lng,
        };

        // console.log(this.usersLocation);
      }
    } else {
      this.SelectDeliveryUnavailable = true;
      this.SelectDeliveryLocation = 1;
      this.currentLocationfet();
      // console.log("no location avl__", this.currentLat);

      this.usersLocation = {};
      this.usersLocation = {
        lat: this.currentLat,
        lng: this.currentLng,
      };
    }

    locations.map((location) => {
      let placeLocation = {};
      placeLocation = {
        lat: location.vendor_dash_details.cus_lat,
        lng: location.vendor_dash_details.cus_lng,
      };

      // console.log('Location 1 ', this.usersLocation);
      location.distance = this.getDistanceBetweenPoints(
        this.usersLocation,
        placeLocation
      ).toFixed(2);
    });
    return locations;
  }

  getDistanceBetweenPoints(start, end) {
    let earthRadius = {
      miles: 3958.8,
      km: 6371,
    };

    let R = 3958.8;
    let lat1 = start.lat;
    let lon1 = start.lng;
    let lat2 = end.lat;
    let lon2 = end.lng;

    let dLat = this.toRad(lat2 - lat1);
    let dLon = this.toRad(lon2 - lon1);
    let a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.toRad(lat1)) *
        Math.cos(this.toRad(lat2)) *
        Math.sin(dLon / 2) *
        Math.sin(dLon / 2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    let d = R * c;

    return d;
  }

  toRad(x) {
    return (x * Math.PI) / 180;
  }

  getCurrentCusData() {
    return new Promise((resolve) => {
      return this.api.WCV2getItem("customers/" + this.user_id).subscribe(
        (res) => {
          resolve(res);
          this.settings.customer = res;

          this.settings.customer = {};
          this.settings.customer = res;
          // this.settings.customer.id = res;

          console.log( '--------------------------???'+this.settings.customer);
          console.log( JSON.stringify(this.settings.customer));
          

          if (this.isVendorLogin == "true") {
            let ax = [];
            ax.push(this.settings.customer);

            var filter = "key";
            var keyword = "_previous_login";

            var _previous_login = ax[0].meta_data.filter(function (obj) {
              return obj[filter] === keyword;
            });
            this.previous_login = [];
            this.previous_login = _previous_login[0].value;
            // console.log('========================'+JSON.stringify(makObj));
            var filter = "key";
            var keyword = "wcfmmp_profile_settings";

            var filteredData = ax[0].meta_data.filter(function (obj) {
              return obj[filter] === keyword;
            });
            let makObj = [];
            makObj = filteredData[0].value;

            console.log("???????????????????SETTINGS==" + JSON.stringify(makObj));


            localStorage.removeItem("storeData");
            this.storage.remove("storeData");
            localStorage.setItem("storeData", JSON.stringify(makObj));
            this.storage.set("storeData", makObj);
          }
          this.commonFiltere();
          return res;
        },
        (err) => {
          console.log(err);
        }
      );
    });
  }

  updateAddress() {
    this.navCtrl.navigateForward("/tabs/account/address/edit-address");
  }

  getDetail(item) {
    // console.log('========>>', item);

    this.vendor.vendor = item;
    this.navCtrl.navigateForward("/tabs/vendor/products");
  }

  async currentLocationfet() {
    console.log("get lat ");
    await this.geolocation
      .getCurrentPosition()
      .then((resp) => {
        this.currentLat = "";
        this.currentLng = "";
        this.currentLat = resp.coords.latitude;
        this.currentLng = resp.coords.longitude;
        // this.getOrderMetrics();

        //     setTimeout(()=>{
        //         this.currentLocationfet();
        //    }, 2000);
      })
      .catch((error) => {
        console.log("Error getting location", error);
      });

    console.log("get lat " + this.currentLat);

    // let watch = this.geolocation.watchPosition();
    // watch.subscribe((data) => {
    //     this.currentLat = '';
    //     this.currentLng = '';
    //     this.currentLat = data.coords.latitude;
    //     this.currentLng = data.coords.longitude;

    //     console.log('Track 1 '+  this.currentLat);

    //     this.getOrderMetrics();

    //         setTimeout(()=>{
    //             this.currentLocationfet();
    //        }, 2000);

    // });

    this.usersLocation = {};

    this.usersLocation = {
      lat: this.currentLat,
      lng: this.currentLng,
    };

    let senddata = {
      order_id: this.order_id_,
      lat: this.currentLat,
      long: this.currentLng,
      status: 1,
    };

    //     console.log('senddata 2'+JSON.stringify(senddata));

    //     await this.api.update_POST_parms('saveOrderLocation' , senddata

    //     ).subscribe(res => {

    //        console.log('??response??'+JSON.stringify(res));
    //     //    this.backgroundGeolocation.finish();

    //    }, err => {
    //        console.log(err);
    //     //    this.backgroundGeolocation.finish();

    //    });
  }

  // ===
  
  dismiss() {
    this.searchPlacesList = false;
    console.log("dismiss");
    // this.searchPlaces = false;
    // this.searchPlacesX = false;
  }

  async chooseItem(item: any) {
    console.log(item);

    this.searchPlaces = true;
    this.geo = item;
    await this.geoCode(this.geo); //convert Address to lat and long
  }

  updateSearch() {
    this.searchPlaces = true;
    this.searchPlacesList = true;

    if (this.autocomplete.query == "") {
      this.autocompleteItems = [];
      return;
    }

    // console.log('New', this.autocomplete.query);

    let me = this;
    this.service.getPlacePredictions(
      {
        input: this.autocomplete.query,
        componentRestrictions: {
          country: "",
        },
      },
      (predictions, status) => {
        me.autocompleteItems = [];

        me.zone.run(() => {
          if (predictions != null) {
            predictions.forEach((prediction) => {
              me.autocompleteItems.push(prediction.description);
            });
          }
        });
      }
    );
  }

  //convert Address string to lat and long
  geoCode(address: any) {
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({ address: address }, (results, status) => {
      this.latitude = results[0].geometry.location.lat();
      this.longitude = results[0].geometry.location.lng();

      this.commonFiltere();
    });
  }

  welcome_() {
    this.navCtrl.navigateForward("/welcome");
  }

  goTo(path) {
    // localStorage.clear();
    // if(path === 'tabs/account/address/edit-address'){
    //    console.log('setState');
    //    localStorage.setItem('accountEdit','true');

    // }
    this.navCtrl.navigateForward(path);
  }

  goToSettings(path) {
    this.navCtrl.navigateForward(path);
  }

  expandItem(): void {
    this.itemExpanded = !this.itemExpanded;
  }

  async getOrderMetrics() {
    await this.api
      .WCV2getAgentDeliveries(
        "ordermetrics?vendor_id=" + this.vendorID + "&range=month"
      )
      .subscribe(
        (res) => {
          console.log(
            "=======getOrderMetrics===============" + res['status']
          );


      if(res['status'] == 'error'){
        console.log('error');
        this.NoMetrics = false;
        this.errorTxt= res['msg'];
        console.log( '--------------------------???'+this.settings.customer);
        console.log( JSON.stringify(this.settings.customer));
       }

       if(res['status'] == 'ok'){
        this.NoMetrics = true;
        let ob = JSON.parse(JSON.stringify(res));
        let obj = Object.values(ob)[2];

        // console.log(obj);

        this.grossSale = obj["gross_sale"];
        this.earing = obj["earing"];
        this.items_purchased = obj["items_purchased"];
        this.number_of_orders = obj["number_of_orders"];

       }

       
        },
        (err) => {
          console.log(err);
        }
      );
  }

  async OrderConfirm(rec, ordrId) {

        await this.api.WCV2getAgentDeliveries('getMyOrders?agent_id='+ this.deliveryAgentID+'&'
        +'order_id=['+ordrId
        +']'
        +'&status=['+
        rec
        +']'
         ).subscribe(res => {

            console.log('======================'+JSON.stringify(res));
            this.deliveryAgentRes = [];
            this.deliveryAgentRes = res["response"];
    
        }, err => {
    
            console.log(err);
    
        });
  }

  async trackDriverLocation() {

    this.backgroundMode.enable();
    console.log(this.count++);
    this.geolocation
      .getCurrentPosition()
      .then((resp) => {
        this.currentLat = "";
        this.currentLng = "";
        this.currentLat = resp.coords.latitude;
        this.currentLng = resp.coords.longitude;
      })
      .catch((error) => {
        console.log("Error getting location", error);
      });

    let watch = this.geolocation.watchPosition();
    watch.subscribe((data) => {
      console.log("Start 3");
      this.currentLat = "";
      this.currentLng = "";
      this.currentLat = data.coords.latitude;
      this.currentLng = data.coords.longitude;
    });

    console.log("Start with Updated Lat======" + this.currentLat);

    console.log(this.order_ids.length);
    console.log(this.order_ids);

    if (this.currentLat !== undefined || this.currentLng !== undefined) {
      let num = this.order_ids.length;
      let Latlist = [];

      for (var i = 0; i < num; i++) {
        Latlist.push(this.currentLat);
      }

      let Lnglist = [];

      for (var i = 0; i < num; i++) {
        Lnglist.push(this.currentLng);
      }

      let statusList = [];

      let val_ = 1;
      for (var i = 0; i < num; i++) {
        statusList.push(val_);
      }

      this.api
        .WCV2getAgentDeliveries(
          "saveOrderLocation?" +
            "order_id=[" +
            this.order_ids +
            "]" +
            "&status=[" +
            statusList +
            "]" +
            "&lat=[" +
            Latlist +
            "]" +
            "&long=[" +
            Lnglist +
            "]"
        )
        .subscribe(
          (res) => {
            console.log("??=====OrderLocation??" + JSON.stringify(res));
            // setInterval(this.trackDriverLocation, 5000);
          },
          (err) => {
            this.orderDetails_ = false;
            this.HttpFailurForDeliveryBoyorderDetails_ = true;
            console.log(err);
          }
        );
    }
  }

  startBackgroundGeolocation() {
    // const config: BackgroundGeolocationConfig = {
    //   desiredAccuracy: 10,
    //   stationaryRadius: 20,
    //   distanceFilter: 30,
    //   debug: true, //  enable this hear sounds for background-geolocation life-cycle.
    //   stopOnTerminate: false, // enable this to clear background location settings when the app terminates
    // };

    console.log("????");

    // this.backgroundGeolocation.configure(config).then(() => {
    //   this.backgroundGeolocation
    //     .on(BackgroundGeolocationEvents.location)
    //     .subscribe((location: BackgroundGeolocationResponse) => {
    //       console.log(location);
    //       alert("location" + location);
    //       this.sendGPS(location);

    //       // IMPORTANT:  You must execute the finish method here to inform the native plugin that you're finished,
    //       // and the background-task may be completed.  You must do this regardless if your operations are successful or not.
    //       // IF YOU DON'T, ios will CRASH YOUR APP for spending too much time in the background.
    //     });
    // });

    // start recording location
    // this.backgroundGeolocation.start();

    // If you wish to turn OFF background-tracking, call the #stop method.
    // this.backgroundGeolocation.stop();
  }

  sendGPS(location) {
    if (location.speed == undefined) {
      location.speed = 0;
    }
    let timestamp = new Date(location.time);

    // to send multiple order_ids
    // to save the origin
    let senddata = {
      order_id: this.order_id_,
      lat: location.latitude,
      long: location.longitude,
      status: 1,
    };

    alert("senddata 2" + senddata);

    this.api.update_POST_parms("saveOrderLocation", senddata).subscribe(
      (res) => {
        alert("API response" + JSON.stringify(res));
        console.log("????" + JSON.stringify(res));
        // this.backgroundGeolocation.finish();
      },
      (err) => {
        alert("API response" + JSON.stringify(err));
        this.orderDetails_ = false;
        this.HttpFailurForDeliveryBoyorderDetails_ = true;
        console.log(err);
        // this.backgroundGeolocation.finish();
      }
    );

    // this.http
    //   .post(
    //     this.gps_update_link, // backend api to post
    //     {
    //       lat: location.latitude,
    //       lng: location.longitude,
    //       speed: location.speed,
    //       timestamp: timestamp
    //     },
    //     {}
    //   )
    //   .then(data => {
    //     console.log(data.status);
    //     console.log(data.data); // data received by server
    //     console.log(data.headers);
    //     this.backgroundGeolocation.finish(); // FOR IOS ONLY
    //   })
    //   .catch(error => {
    //     console.log(error.status);
    //     console.log(error.error); // error message as string
    //     console.log(error.headers);
    //     this.backgroundGeolocation.finish(); // FOR IOS ONLY
    //   });
    //   }
  }

  async view_metrics() {
    let sd = this.date_start.slice(0, 10);
    let ed = this.end_start.slice(0, 10);

    await this.api
      .WCV2getAgentDeliveries(
        "ordermetrics?vendor_id=" +
          this.vendorID +
          "&range=custom" +
          "&start_date=" +
          sd +
          "&end_date=" +
          ed
      )
      .subscribe(
        (res) => {
          console.log(
            "======================" + JSON.stringify(res["response"])
          );

          this.custom_dates_view = true;

          let sale1 = JSON.stringify(res["response"]["gross_sale"]);
          let sale2 = JSON.stringify(res["response"]["earing"]);
          let sale3 = JSON.stringify(res["response"]["items_purchased"]);
          let sale4 = JSON.stringify(res["response"]["number_of_orders"]);

          // let js = this.json2array(res);

          // let ob = JSON.parse(res['response']);
          // let obj =  Object.values(ob);
          // let obj =  Object.values(ob)[2];
          this.filterSale_ = [
            {
              gross_sale: sale1,
              earing: sale2,
              items_purchased: sale3,
              number_of_orders: sale4,
            },
          ];
          console.log(
            "========sale1==============" + JSON.stringify(this.filterSale_)
          );
          // console.log('======================'+ob["'response'"]);

          if (JSON.stringify(res["status"]) == "ok") {
            console.log("Yeyy");
          }

          if (JSON.stringify(res["status"]) == "error") {
            console.log("Nopes");
          }
          // this.grossSale = obj['gross_sale'];
          // this.earing = obj['gross_sale'];
          // this.items_purchased = obj['items_purchased'];
          // this.number_of_orders = obj['number_of_orders'];
        },
        (err) => {
          console.log(err);
        }
      );
  }

  json2array(json) {
    var result = [];
    var keys = Object.keys(json);
    keys.forEach(function (key) {
      result.push(json[key]);
    });
    return result;
  }

  async getOrders__() {
    await this.api
      .WCV2getAgentDeliveries("getvendororders?vendor_id=" + this.vendorID)
      .subscribe(
        (res) => {
          this.order_status_ven = [
            { label: "ready-to-pick-up" },
            { label: "processing" },
            { label: "completed" },
            { label: "on-the-way" },
            { label: "on-hold" },
            { label: "cancelled" },
            { label: "refunded" },
            { label: "payment-pending" },
          ];
          let ven = JSON.stringify(res["response"]);

          let ax: {};
          ax = ven;

          this.VendorOrders_ = res["response"];
          // this.VendorOrders_.push(ax);

          console.log("VENDORS=========" + this.VendorOrders_);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  //       range=month(current month)
  // range=last_month
  // range=7day
  // range=year
  // https://fetch.betaplanets.com/wp-json/mobileapi/v1/ordermetrics?vendor_id=7&range=month
  // For custom dates
  // https://fetch.betaplanets.com/wp-json/mobileapi/v1/ordermetrics?vendor_id=7&range=custom&start_date=2020-04-21&end_date=2020-05-05

  getOrder_Detail(order, status) {
    console.log(order);
    console.log(order.order_detail.order_id);

    let orderX = {
      id: order.order_detail.order_id,
      status: status,
      customer_note: "",
      date_created: order.order_detail._paid_date,
      total: order.order_detail._order_total,
      discount_total: order.order_detail._cart_discount,

      shipping_tax: order.order_detail._order_shipping_tax,
      shipping_total: order.order_detail._order_shipping,
      total_tax: order.order_detail._order_tax,

      billing: {
        address_1: order.order_detail._billing_address_1,
        address_2: order.order_detail._billing_address_2,
        city: order.order_detail._billing_city,
        company: order.order_detail._billing_company,
        country: order.order_detail._billing_country,
        email: order.order_detail._billing_email,
        first_name: order.order_detail._billing_first_name,
        last_name: order.order_detail._billing_last_name,
        phone: order.order_detail._billing_phone,
        postcode: order.order_detail._billing_postcode,
        state: order.order_detail._billing_state,
      },

      shipping: {
        address_1: order.order_detail._shipping_address_1,
        address_2: order.order_detail._shipping_address_2,
        city: order.order_detail._shipping_city,
        company: order.order_detail._shipping_company,
        country: order.order_detail._shipping_country,
        first_name: order.order_detail._shipping_first_name,
        last_name: order.order_detail._shipping_last_name,
        postcode: order.order_detail._shipping_postcode,
        state: order.order_detail._shipping_state,
      },

      line_items: [
        {
          name: order.order_detail.product_details.product_name,
          price: order.order_detail.product_details.product_price,
          quantity: order.order_detail.product_details._qty,
          total: order.order_detail.product_details.item_total,
        },
      ],
    };

    localStorage.removeItem("orderData");
    this.storage.remove("orderData");
    localStorage.setItem("orderData", JSON.stringify(order));
    this.storage.set("orderData", order);

    let navigationExtras: NavigationExtras = {
      queryParams: {
        order: orderX,
      },
    };
    this.navCtrl.navigateForward(
      "/vendor-edit-order/" + order.order_detail.order_id
    );
  }

  async refreshOrderStatus(o, sts) {
    this.Messg = "";
    console.log(o);

    console.log(o.order_detail.order_id);

    await this.api
      .WCV2getAgentDeliveries(
        "updateorderstatus?vendor_id=" +
          this.vendorID +
          "&order_id=" +
          o.order_detail.order_id +
          "&status=" +
          sts +
          "&assign_delivery_agent=" +
          o.order_detail.delivery_agent_id +
          "&delivery_time=" +
          o.order_detail.agent_time_to_deliver
      )
      .subscribe(
        (res) => {
          console.log("=====================", res);
          this.Messg = res["msg"];
          this.presentToast();

          //  if(){

          //  }
        },
        (err) => {
          console.log(err);
        }
      );
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: this.Messg,
      duration: 4000,
    });
    toast.present();
  }

  getVal(i) {
    this.store_name = i.display_name;
  }

  Phn_dialer2(Numbr) {
    window.open(`tel:` + Numbr, "_system");
  }


  mySearchFunctionTable(){
           
            // const toast = await this.toastCtrl.create({  
            //     message: 'It is a Toast Notification',  
            //     duration: 5000  
            //   });  
            //   toast.present();
            var input, filter, table, tr, td, i, txtValue,td2,txt;
            input = document.getElementById("searchinput");
            filter = input.value.toUpperCase();
            table = document.getElementById("myTable");
            tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0];
                td2 =tr[i].getElementsByTagName("td")[1];  
                if (td || td2) {
                  txtValue = td.textContent || td.innerText;
                  txt=td2.textContent || td2.innerText;
                  if (txtValue.toUpperCase().indexOf(filter) > -1 || txt.toUpperCase().indexOf(filter)>-1) {
                    tr[i].style.display = "";
                  } else {
                    tr[i].style.display = "none";
                  }
                }       
              }
        }




        async presentOptions() {
          const alert = await this.alertCtrl.create({
            message: 'Signin or Signup on App',
            buttons: [
              {
                text: 'Cancel',
                role: 'cancel',
                handler: () => {
                  console.log('Cancel clicked');
                }
              },
              {
                text: 'Login',
                handler: () => {
                  this.navCtrl.navigateForward("/tabs/account/register");
                }
              }
            ]
         });
         await alert.present(); 
      }




      onInput() {

      
        this.filter_.page = 1;
        this.filter_.q = this.searchInput;
        if (this.searchInput.length) {
            // this.getWCFMVendors();
            let ven = this.vendors;

            console.log(this.vendors);
            console.log(this.searchInput);

            this.vendors_s = ven.filter((d) => {
             d.vendor_products.filter((Initem) => {

              console.log(Initem);
              
                return Initem.vendor_products.filter((Initem) => {

                
                  return (Initem.product_name == this.searchInput) 
             });

                
                // return (Initem.product_name == this.searchInput) 
           });
          


            // this.vendors_s = ven.filter(item => item.vendor_products.product_name === this.searchInput);

         
            
            // this.products_s = true;
        } );

        console.log('-------------'+JSON.stringify(this.vendors_s));

      
    }
    

  }




  getVendors_sub(){

    let senddata = {
      user_id: this.vendorID,
    };

    this.api.update_POST_parms("getsubscription", senddata).subscribe(
      (res) => {

        this.membership_status = true;
        this.Subscript = res['response']['current_membership'];
        this.ActiveLast = res['response']['current_membership'];
        console.log("????" + JSON.stringify(this.Subscript));
        // this.backgroundGeolocation.finish();
      },
      (err) => {
        console.log(err);
      }
    );
  }

}
