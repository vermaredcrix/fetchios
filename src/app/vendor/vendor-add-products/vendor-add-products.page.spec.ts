import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VendorAddProductsPage } from './vendor-add-products.page';

describe('VendorAddProductsPage', () => {
  let component: VendorAddProductsPage;
  let fixture: ComponentFixture<VendorAddProductsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorAddProductsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VendorAddProductsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
