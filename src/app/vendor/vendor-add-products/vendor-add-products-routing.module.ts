import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VendorAddProductsPage } from './vendor-add-products.page';

const routes: Routes = [
  {
    path: '',
    component: VendorAddProductsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VendorAddProductsPageRoutingModule {}
