import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ApiService } from '../../api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Settings } from './../../data/settings';
import { FormBuilder, FormArray, Validators } from '@angular/forms';


@Component({
  selector: 'app-driver-account-create',
  templateUrl: './driver-account-create.page.html',
  styleUrls: ['./driver-account-create.page.scss'],
})
export class DriverAccountCreatePage implements OnInit {
  vendorID: any;
  isVendorLogin: any;
  account_detail :any ;
  availabilityDrv =true;
  Error_View:boolean = false;
  errorTxt = '';
  A_username = '';
  OkTxt = '';
  AccountCreated = false;

  constructor(
    public api: ApiService,
    private storage: Storage,
    public route: ActivatedRoute,
    public settings: Settings,
    private fb: FormBuilder
  ) {

    this.account_detail = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      email: ['', Validators.email],
      phone: ['', Validators.required],
      availability_status: [true, ],

    });

    this.account_detail.value.availability_status = true;


    this.vendorID = localStorage.getItem('vendorID');
    this.isVendorLogin = localStorage.getItem('isVendorLogin');

    this.storage.get('isVendorLogin').then((val) => {
        this.isVendorLogin = val;
    });

    this.storage.get('vendorID').then((val) => {
        this.vendorID = val;
    });

   }

  ngOnInit() {
  }
  // https://fetch.betaplanets.com/wp-json/mobileapi/v1/?vendor_id=7&username=testuser1&password=12345&email=testuser@gmail.com&phone=34243243&first_name=test&last_name=user&availability_status=available

  async AddDriver(){

    this.errorTxt = '';
    this.A_username = '';
    console.log(this.account_detail);

    let sen_data = {
      vendor_id : this.vendorID,
      // user_id : this.vendorID,
      username : this.account_detail.value.username,
      password : this.account_detail.value.password,
      email: this.account_detail.value.email,
      phone: this.account_detail.value.phone,
      first_name: this.account_detail.value.first_name,
      last_name: this.account_detail.value.last_name,
      availability_status: this.account_detail.value.availability_status
    }
    // https://fetch.betaplanets.com/wp-json/mobileapi/v1/getsubscription

      await this.api.update_POST_parms('getsubscription', sen_data).subscribe(res => {
       console.log('=====================', res);
       console.log(res['status']);
       if(res['status'] = 'error'){
        console.log('error');
        this.errorTxt= res['msg'];
       }

       if(res['status'] = 'ok'){
        this.errorTxt = '';
        this.OkTxt = '';
        this.OkTxt= res['msg'];
        this.AccountCreated = true;
        this.A_username = this.account_detail.value.username;

       }

      
 
   }, err => {
    
       console.log(err);
 
   });


 

  }


}
