import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { DriverAccountCreatePageRoutingModule } from './driver-account-create-routing.module';

import { DriverAccountCreatePage } from './driver-account-create.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DriverAccountCreatePageRoutingModule,
    TranslateModule,
    ReactiveFormsModule
  ],
  declarations: [DriverAccountCreatePage]
})
export class DriverAccountCreatePageModule {}
