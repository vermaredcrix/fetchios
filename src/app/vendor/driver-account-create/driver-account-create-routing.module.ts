import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DriverAccountCreatePage } from './driver-account-create.page';

const routes: Routes = [
  {
    path: '',
    component: DriverAccountCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DriverAccountCreatePageRoutingModule {}
