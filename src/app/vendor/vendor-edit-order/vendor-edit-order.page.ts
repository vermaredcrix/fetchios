import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ApiService } from '../../api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Settings } from './../../data/settings';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-vendor-edit-order',
  templateUrl: './vendor-edit-order.page.html',
  styleUrls: ['./vendor-edit-order.page.scss'],
})
export class VendorEditOrderPage implements OnInit {
  vendorID: any;
  id: any;
  isVendorLogin: any;
  orders:any = [];
  Order_Status:any;
  Agent_id:any;
  AgentsDetails:any;
  SelectedAgent : any;
  Messg = '';
  orders_:any;
  isSub:any;


  constructor(
    public api: ApiService,
    private storage: Storage,
    public route: ActivatedRoute,
    public settings: Settings,
    public toastController: ToastController
  ) {

    

    this.vendorID = localStorage.getItem('vendorID');
    this.isVendorLogin = localStorage.getItem('isVendorLogin');

    this.storage.get('isVendorLogin').then((val) => {
        this.isVendorLogin = val;
    });

    this.storage.get('vendorID').then((val) => {
        this.vendorID = val;
    });



    this.orders_= JSON.parse(localStorage.getItem('orderData'));

    this.storage.get('orderData').then((val) => {
        this.orders_ = val;
    });

    this.id = this.route.snapshot.paramMap.get('id');

   

    // this.orders = Object.keys(this.orders_).map(key => this.orders[key])

    let ax = [];
    ax = this.orders_;

    this.orders = [];
    this.orders.push(ax);
    console.log(this.orders);

    this.getAllAgents();


   }

  ngOnInit() {
  }

  async refreshStatus(){
    this.Messg = '';
    console.log(this.orders);

    console.log(this.orders[0].order_detail.order_id);
    
    

    await this.api.WCV2getAgentDeliveries('updateorderstatus?vendor_id='
     + this.vendorID 
    + '&order_id=' + this.orders[0].order_detail.order_id
    + '&status=' + this.Order_Status
    + '&assign_delivery_agent=' + this.orders[0].order_detail.delivery_agent_id
    + '&delivery_time=' + this.orders[0].order_detail.agent_time_to_deliver
    
    ).subscribe(res => {
      console.log('=====================', res);
      this.Messg = res["msg"];
      this.presentToast();
 
    //  if(){
       
    //  }

  }, err => {
   
      console.log(err);

  });


  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: this.Messg,
      duration: 4000
    });
    toast.present();
  }


  async getAllAgents(){

    this.isSub = localStorage.getItem('isSubscribed');
    this.storage.get('isSubscribed').then((val) => {
        this.isSub = val;
    });


    if(this.isSub == 'true'){
      await this.api.WCV2getAgentDeliveries('alldeliveryagent').subscribe(res => {
    

        this.AgentsDetails = res['response'];
        console.log('=====================', this.AgentsDetails);
    }, err => {
     
        console.log(err);
  
    });
    }



  }


  AgentsDetails_(){
    
    console.log('Log -'+this.SelectedAgent);
    
  }




}
