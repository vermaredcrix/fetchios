


import { Component, OnInit ,ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Chart } from 'chart.js';
import { PipeCollector } from '@angular/compiler/src/template_parser/binding_parser';


@Component({
  selector: 'app-reports',
  templateUrl: './reports.page.html',
  styleUrls: ['./reports.page.scss'],
})
export class ReportsPage implements OnInit {
  @ViewChild("barChart",{static:true}) barCanvas;

 
  dayData=[]=new Array();
  cdata:any;
  timenow:any;
 

 data:any;
 data_2:any;


  labelList=[]=new Array();
  currentMonth:any;
  currentMonthDays:any;
  tDate:any;
  size=[]=new Array(new Date(new Date().getFullYear(),new Date().getMonth()+1,0).getDate());
  static gross = new Array(new Date(new Date().getFullYear(),new Date().getMonth()+1,0).getDate());
  static earning=new Array(new Date(new Date().getFullYear(),new Date().getMonth()+1,0).getDate());
  prevMonthGrossList=new Array(new Date(new Date().getFullYear(),new Date().getMonth(),0).getDate())
  prevMonthEarningList=new Array(new Date(new Date().getFullYear(),new Date().getMonth(),0).getDate())
  
  random=12;

  month = []= new Array();
  


  d = new Date();

  

  constructor(public http: HttpClient) { 
    this.getMonths();



  }

  getMonths(){
    this.month[0]="January";
    this. month[1] = "February";
    this.month[2] = "March";
    this. month[3] = "April";
    this. month[4] = "May";
    this. month[5] = "June";
    this. month[6] = "July";
    this.month[7] = "August";
    this.month[8] = "September";
    this.month[9] = "October";
    this.month[10] = "November";
    this.month[11] = "December";
    console.log("Today Date",this.month);
    this.currentMonth = this.month[this.d.getMonth()];

    // Array.apply(null, this.gross).map(Number.prototype.valueOf,0);
    // Array.apply(null, this.earning).map(Number.prototype.valueOf,0);
    //We got the Month
    console.log("Month",this.currentMonth);
    var e=this.d.getFullYear();
    this.currentMonthDays=new Date(new Date().getFullYear(),new Date().getMonth()+1,0).getDate();
    //We got the Days of specific Month
    console.log("This month has :",this.currentMonthDays," Days");
    for(var i=0;i<this.currentMonthDays;i++){
      this.labelList.push((i+1)+" "+this.currentMonth);
    }
    console.log("Month Days List:",this.labelList);

    // for(let i=0;i<12;i++){
    //   this.monthsNames.push(this.months[i].name);
    // }
    // console.log("Month List",this.monthsNames);

    
  }
  ngOnInit(){
    this.dayData=[];

    this.callData();
    this.createCharts();
  }

 
  callData(){
    this.http.get('https://fetch.betaplanets.com/wp-json/mobileapi/v1/ordermetrics?vendor_id=7&range=day')
    .subscribe(res=>{
        console.log("Res :",res['response']);
        this.cdata=res['response'];
        console.log("C Data Month",this.cdata['gross_sale']);
     
        // setTimeout(function(){
        //   this.fun();
        // },24*60*60*60);
        

        //this.gross[20]=this.cdata['gross_sale'];
        // this.gross.splice(this.d.getDate()-1,0,this.cdata['gross_sale']);
        // this.earning.splice(this.d.getDate()-1,0,this.cdata['earing']);
        ReportsPage.gross.splice(this.d.getDate()-1,0,this.cdata['gross_sale']);
        ReportsPage.earning.splice(this.d.getDate()-1,0,this.cdata['earing']);
        if(this.currentMonthDays+1>this.currentMonthDays){

          this.prevMonthGrossList=ReportsPage.gross;
         this.prevMonthEarningList=ReportsPage.earning;
        }
        
        
    });
   
  }
  // async fun(){
  //   this.gross.push(this.cdata['gross_sale']);
  //   this.earning.push(this.cdata['earing']);
  // }

  prevData(){


  }
  createCharts(){
 
    this.barCanvas = new Chart(this.barCanvas.nativeElement, {
      type: "line",
      data: {
        labels: this.labelList,
        datasets: [
          {
            label: "# of Gross Sales",
            data:  ReportsPage.gross,
            backgroundColor: [
              "rgba(255, 99, 132, 0.2)",
            
            ],
            borderColor: [
              "rgba(255,99,132,1)",
             
      
            ],
            borderWidth: 1,
          },
          {
            label: "# of Earnings",
            data:  ReportsPage.earning,
            backgroundColor: [
              "rgba(54, 162, 235, 0.2)",
            
            ],
            borderColor: [
      
              "rgba(54, 162, 235, 1)",
      
            ],
            borderWidth: 1,
          }
        ]
      },
      options: {
        scales: {
          yAxes: [
            {
              ticks: {
                beginAtZero: true
              }
            }
          ]
        }
      }
    });


  }

}
