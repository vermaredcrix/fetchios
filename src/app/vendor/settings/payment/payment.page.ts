import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../api.service';
import { Settings } from './../../../data/settings';
import { LoadingController} from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.page.html',
  styleUrls: ['./payment.page.scss'],
})
export class PaymentPage implements OnInit {
  store_details_:any;
  store_details:any;
  store_details__:any;
  vendorID: any;
  isVendorLogin: any;
  Mess = '';
  paymentMethods = [];
  PayPalInfo : boolean = false;

  constructor(
    public settings: Settings,
    public loadingController: LoadingController, 
    public api: ApiService,
    private storage: Storage,
  ) { 

    this.load_storage_data();
  }

  ngOnInit() {

    if(this.store_details.payment.method == 'bank_transfer'){
      this.PayPalInfo = false;
    }

    if(this.store_details.payment.method == 'paypal'){
      this.PayPalInfo = true;
    }
  }

  load_storage_data(){
    this.vendorID = localStorage.getItem('vendorID');
    this.isVendorLogin = localStorage.getItem('isVendorLogin');

    this.storage.get('isVendorLogin').then((val) => {
        this.isVendorLogin = val;
    });

    this.storage.get('vendorID').then((val) => {
        this.vendorID = val;
    });


    this.store_details_= JSON.parse(localStorage.getItem('storeData'));

    this.storage.get('storeData').then((val) => {
        this.store_details_ = val;
    });

    let ax = [];
    ax = this.store_details_;

    this.store_details__ = [];
    this.store_details__.push(ax);

      console.log(this.store_details__[0]);

      this.store_details = this.store_details__[0];

      this.paymentMethods = [
        {value: "bank_transfer" , label : 'Bank Transfer'},
        {value: "paypal" , label : 'PayPal'},

      ]
  }



  async updateVendorDetail() {
    this.Mess = '';
    const loading = await this.loadingController.create({
        message: 'Loading...',
        translucent: true,
        cssClass: 'custom-class custom-loading'
    });
    await loading.present();


    let store_update = 
      {
        vendor_id : this.vendorID,
        payment_fees_title : this.store_details.payment.payment_fees_title,
        payment_fees : this.store_details.payment.payment_fees,
        method : this.store_details.payment.method,

        ac_name: this.store_details.payment.bank.ac_name,
        ac_number: this.store_details.payment.bank.ac_number,
        bank_addr: this.store_details.payment.bank.bank_addr,
        bank_name: this.store_details.payment.bank.bank_name,
        iban: this.store_details.payment.bank.iban,
        ifsc: this.store_details.payment.bank.ifsc,
        routing_number: this.store_details.payment.bank.routing_number,
        swift: this.store_details.payment.bank.swift,

        paypal_email : this.store_details.payment.paypal.email

      }
    console.log(store_update);
    
    await this.api.update_POST_parms('updatevendorstore', store_update).subscribe(res => {
        console.log('====', res);
        this.Mess = 'Info updated.';
        this.loadData();
        loading.dismiss();

    }, err => {
      this.Mess = 'Info not updated.';
      loading.dismiss();
        console.log(err);
    });


}


async loadData() {
  var loc = await this.getCurrentCusData();

}

getCurrentCusData() {
  return new Promise(resolve => {

      return this.api.WCV2getItem('customers/' + this.vendorID).subscribe(res => {
          this.settings.customer = res;
          resolve(res)
          console.log(this.settings.customer);

          if (this.isVendorLogin = true) {

          let ax = [];
          ax.push(this.settings.customer);
          var filter = "key";
          var keyword = "wcfmmp_profile_settings";
      
          var filteredData = ax[0].meta_data.filter(function(obj) {
            return obj[filter] === keyword;
          });
            let makObj = [];
            makObj = filteredData[0].value;
          console.log(makObj);


          localStorage.removeItem('storeData');
          this.storage.remove('storeData'); 
          localStorage.setItem('storeData', JSON.stringify(makObj));
          this.storage.set('storeData', makObj); 

          console.log('res=========',makObj['store_name']);
          

      }
          return res;

      }, err => {
          console.log(err);
      });

  });
}


filterPaymentType(){
  console.log(this.store_details.payment.method);

  if(this.store_details.payment.method == 'paypal'){
    this.PayPalInfo = true;
  }

  if(this.store_details.payment.method == 'bank_transfer'){
    this.PayPalInfo = false;
  }

  
  
}


}
