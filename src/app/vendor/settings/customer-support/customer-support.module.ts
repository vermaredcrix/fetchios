import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { CustomerSupportPageRoutingModule } from './customer-support-routing.module';

import { CustomerSupportPage } from './customer-support.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CustomerSupportPageRoutingModule,
    TranslateModule
  ],
  declarations: [CustomerSupportPage]
})
export class CustomerSupportPageModule {}
