import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../api.service';
import { Settings } from './../../../data/settings';
import { LoadingController} from '@ionic/angular';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-store-hours',
  templateUrl: './store-hours.page.html',
  styleUrls: ['./store-hours.page.scss'],
})
export class StoreHoursPage implements OnInit {
  store_details_:any;
  store_details:any;
  store_details__:any;
  vendorID: any;
  isVendorLogin: any;
  Mess = '';
  paymentMethods = [];
  enableHours:any;
  off_days : any;

  check_mon:any;
  check_tue:any;
  check_wed:any;
  check_thr:any;
  check_fri:any;
  check_sat:any;
  check_sun:any;

  constructor(
    public settings: Settings,
    public loadingController: LoadingController, 
    public api: ApiService,
    private storage: Storage,
  ) { 
    this.load_storage_data();
  }

  ngOnInit() {

    if(this.store_details.wcfm_store_hours.enable == 'yes'){
      this.enableHours = true;
    }
    if(this.store_details.wcfm_store_hours.enable == 'no'){
      this.enableHours = false;
    }
    if(this.store_details.wcfm_store_hours.off_days == 'no'){
      this.off_days = false;
    }
    if(this.store_details.wcfm_store_hours.off_days != 'no'){
      this.off_days = true;

      
      // let val2 = this.store_details.wcfm_store_hours.find(item => item.off_days == "0");
      // console.log(val2);
      
    //   var exists0 = Object.keys(this.store_details.wcfm_store_hours.off_days).some(function(k) {
    //     return this.store_details.wcfm_store_hours.off_days[k] === "0";
    // });

//     var exists1 = Object.keys(this.store_details.wcfm_store_hours.off_days).some(function(k) {
//       return this.store_details.wcfm_store_hours.off_days[k] === "1";
//   });

//   var exists2 = Object.keys(this.store_details.wcfm_store_hours.off_days).some(function(k) {
//     return this.store_details.wcfm_store_hours.off_days[k] === "2";
// });

// var exists3 = Object.keys(this.store_details.wcfm_store_hours.off_days).some(function(k) {
//   return this.store_details.wcfm_store_hours.off_days[k] === "3";
// });

// var exists4 = Object.keys(this.store_details.wcfm_store_hours.off_days).some(function(k) {
//   return this.store_details.wcfm_store_hours.off_days[k] === "4";
// });

// var exists5 = Object.keys(this.store_details.wcfm_store_hours.off_days).some(function(k) {
//   return this.store_details.wcfm_store_hours.off_days[k] === "5";
// });

// var exists6 = Object.keys(this.store_details.wcfm_store_hours.off_days).some(function(k) {
//   return this.store_details.wcfm_store_hours.off_days[k] === "6";
// });

// console.log('One========',exists0);
// console.log('One========',exists1);
// console.log('One========',exists2);
// console.log('One========',exists3);
// console.log('One========',exists4);
// console.log('One========',exists5);
// console.log('Sun========',exists6);




    }
    
  }



  load_storage_data(){
    this.vendorID = localStorage.getItem('vendorID');
    this.isVendorLogin = localStorage.getItem('isVendorLogin');

    this.storage.get('isVendorLogin').then((val) => {
        this.isVendorLogin = val;
    });

    this.storage.get('vendorID').then((val) => {
        this.vendorID = val;
    });


    this.store_details_= JSON.parse(localStorage.getItem('storeData'));

    this.storage.get('storeData').then((val) => {
        this.store_details_ = val;
    });

    let ax = [];
    ax = this.store_details_;

    this.store_details__ = [];
    this.store_details__.push(ax);

      console.log(this.store_details__[0]);

      this.store_details = this.store_details__[0];

      this.paymentMethods = [
        {value: "bank_transfer" , label : 'Bank Transfer'},
        {value: "paypal" , label : 'PayPal'},

      ]
  }



  async updateVendorDetail() {
    this.Mess = '';
    const loading = await this.loadingController.create({
        message: 'Loading...',
        translucent: true,
        cssClass: 'custom-class custom-loading'
    });
    await loading.present();

    console.log(this.check_mon);
    console.log(this.check_tue);
    


    let store_update = 
      {
        vendor_id : this.vendorID,
        payment_fees_title : this.store_details.payment.payment_fees_title,
        payment_fees : this.store_details.payment.payment_fees,
        method : this.store_details.payment.method,

        ac_name: this.store_details.payment.bank.ac_name,
        ac_number: this.store_details.payment.bank.ac_number,
        bank_addr: this.store_details.payment.bank.bank_addr,
        bank_name: this.store_details.payment.bank.bank_name,
        iban: this.store_details.payment.bank.iban,
        ifsc: this.store_details.payment.bank.ifsc,
        routing_number: this.store_details.payment.bank.routing_number,
        swift: this.store_details.payment.bank.swift,

        paypal_email : this.store_details.payment.paypal.email

      }
    console.log(store_update);
    
    await this.api.update_POST_parms('updatevendorstore', store_update).subscribe(res => {
        console.log('====', res);
        this.Mess = 'Info updated.';
        this.loadData();
        loading.dismiss();

    }, err => {
      this.Mess = 'Info not updated.';
      loading.dismiss();
        console.log(err);
    });


}


async loadData() {
  var loc = await this.getCurrentCusData();

}

getCurrentCusData() {
  return new Promise(resolve => {

      return this.api.WCV2getItem('customers/' + this.vendorID).subscribe(res => {
          this.settings.customer = res;
          resolve(res)
          console.log(this.settings.customer);

          if (this.isVendorLogin = true) {

          let ax = [];
          ax.push(this.settings.customer);
          var filter = "key";
          var keyword = "wcfmmp_profile_settings";
      
          var filteredData = ax[0].meta_data.filter(function(obj) {
            return obj[filter] === keyword;
          });
            let makObj = [];
            makObj = filteredData[0].value;
          console.log(makObj);


          localStorage.removeItem('storeData');
          this.storage.remove('storeData'); 
          localStorage.setItem('storeData', JSON.stringify(makObj));
          this.storage.set('storeData', makObj); 

          console.log('res=========',makObj['store_name']);
          

      }
          return res;

      }, err => {
          console.log(err);
      });

  });
}

updateStoreHours(ev){
  console.log(ev);
  
}

updateStoreOffs(ev){
  console.log(ev);
  
}


}
