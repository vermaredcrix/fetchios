import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { StoreHoursPageRoutingModule } from './store-hours-routing.module';

import { StoreHoursPage } from './store-hours.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StoreHoursPageRoutingModule,
    TranslateModule
  ],
  declarations: [StoreHoursPage]
})
export class StoreHoursPageModule {}
