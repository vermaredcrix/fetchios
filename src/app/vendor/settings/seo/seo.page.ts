import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../api.service';
import { Settings } from './../../../data/settings';
import { LoadingController} from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-seo',
  templateUrl: './seo.page.html',
  styleUrls: ['./seo.page.scss'],
})
export class SeoPage implements OnInit {
  store_details_:any;
  store_details:any;
  store_details__:any;
  vendorID: any;
  isVendorLogin: any;
  Mess = '';
  paymentMethods = [];
  seoData :any = {};

  constructor(
    public settings: Settings,
    public loadingController: LoadingController, 
    public api: ApiService,
    private storage: Storage,
  ) {
    this.load_storage_data();
   }

  ngOnInit() {

 
    

  }

  load_storage_data(){
    this.vendorID = localStorage.getItem('vendorID');
    this.isVendorLogin = localStorage.getItem('isVendorLogin');

    this.storage.get('isVendorLogin').then((val) => {
        this.isVendorLogin = val;
    });

    this.storage.get('vendorID').then((val) => {
        this.vendorID = val;
    });


    this.store_details_= JSON.parse(localStorage.getItem('storeData'));

    this.storage.get('storeData').then((val) => {
        this.store_details_ = val;
    });

    console.log(this.store_details_);
    
    console.log(JSON.stringify(this.store_details_));

    let ax = [];
    ax = this.store_details_;

    this.store_details__ = [];
    this.store_details__.push(ax);

      console.log(this.store_details__[0]);

      this.store_details = this.store_details__[0];

      this.paymentMethods = [
        {value: "bank_transfer" , label : 'Bank Transfer'},
        {value: "paypal" , label : 'PayPal'},

      ]


      this.seoData = 
      {   title : this.store_details.store_seo['wcfmmp-seo-meta-title'],
         des : this.store_details.store_seo['wcfmmp-seo-meta-desc'],
         keyword : this.store_details.store_seo['wcfmmp-seo-meta-keywords'],
   
         fb_title : this.store_details.store_seo['wcfmmp-seo-og-title'],
         fb_des : this.store_details.store_seo['wcfmmp-seo-og-desc'],
   
         tw_title : this.store_details.store_seo['wcfmmp-seo-twitter-title'],
         tw_des : this.store_details.store_seo['wcfmmp-seo-twitter-desc'],
     } ;
       
   
   
   
       console.log(this.store_details.store_seo['wcfmmp-seo-meta-title']);
       console.log(this.store_details.store_seo['wcfmmp-seo-meta-desc']);
    
       
       
  }



  async updateVendorDetail() {
    this.Mess = '';
    const loading = await this.loadingController.create({
        message: 'Loading...',
        translucent: true,
        cssClass: 'custom-class custom-loading'
    });
    await loading.present();


    let store_update = 
      {
        vendor_id : this.vendorID,
        'wcfmmp-seo-meta-title' : this.seoData['title'],
        'wcfmmp-seo-meta-desc' : this.seoData['des'],
        'wcfmmp-seo-meta-keywords' : this.seoData['keyword'],

        'wcfmmp-seo-og-title': this.seoData['fb_title'],
        'wcfmmp-seo-og-desc': this.seoData['fb_des'],
        'wcfmmp-seo-twitter-title':this.seoData['tw_title'],
        'wcfmmp-seo-twitter-desc':this.seoData['tw_des'],

      }
    console.log(store_update);
    
    await this.api.update_POST_parms('updatevendorstore', store_update).subscribe(res => {
        console.log('====', res);
        this.Mess = 'Info updated.';
        this.loadData();
        loading.dismiss();

    }, err => {
      this.Mess = 'Info not updated.';
      loading.dismiss();
        console.log(err);
    });


}


async loadData() {
  var loc = await this.getCurrentCusData();

}

getCurrentCusData() {
  return new Promise(resolve => {

      return this.api.WCV2getItem('customers/' + this.vendorID).subscribe(res => {
          this.settings.customer = res;
          resolve(res)
          console.log(this.settings.customer);

          if (this.isVendorLogin = true) {

          let ax = [];
          ax.push(this.settings.customer);
          var filter = "key";
          var keyword = "wcfmmp_profile_settings";
      
          var filteredData = ax[0].meta_data.filter(function(obj) {
            return obj[filter] === keyword;
          });
            let makObj = [];
            makObj = filteredData[0].value;
          console.log(makObj);


          localStorage.removeItem('storeData');
          this.storage.remove('storeData'); 
          localStorage.setItem('storeData', JSON.stringify(makObj));
          this.storage.set('storeData', makObj); 

          // console.log('res=========',makObj['store_name']);
          

      }
          return res;

      }, err => {
          console.log(err);
      });

  });
}




}
