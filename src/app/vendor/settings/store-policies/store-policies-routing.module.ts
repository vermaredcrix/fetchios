import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StorePoliciesPage } from './store-policies.page';

const routes: Routes = [
  {
    path: '',
    component: StorePoliciesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StorePoliciesPageRoutingModule {}
