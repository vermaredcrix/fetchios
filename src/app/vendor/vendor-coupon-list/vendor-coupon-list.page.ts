import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';
import { Storage } from '@ionic/storage';
import { VendorCouponAddPage } from '../vendor-coupon-add/vendor-coupon-add.page';
import { ModalController } from '@ionic/angular';
import { Settings } from './../../data/settings';


@Component({
  selector: 'app-vendor-coupon-list',
  templateUrl: './vendor-coupon-list.page.html',
  styleUrls: ['./vendor-coupon-list.page.scss'],
})
export class VendorCouponListPage implements OnInit {
  loader: boolean = false;
  coupons:any;
  vendorID: any;
  isVendorLogin: any;

  constructor(
    public api: ApiService,
    private storage: Storage,
    public modalController: ModalController,
    public settings: Settings, 
  ) { 

    this.vendorID = localStorage.getItem('vendorID');
    this.isVendorLogin = localStorage.getItem('isVendorLogin');

    this.storage.get('isVendorLogin').then((val) => {
        this.isVendorLogin = val;
    });

    this.storage.get('vendorID').then((val) => {
        this.vendorID = val;
    });
  }

  ngOnInit() {

    this.getCoupons__()
  }


  async getCoupons__() {
    this.loader = true;
    await this.api.WCV2getAgentDeliveries('couponlisting?vendor_id=' + this.vendorID).subscribe(res => {
        this.loader = false;

        this.coupons = res["response"];
        console.log(this.coupons);


    }, err => {
        this.loader = false;
        console.log(err);

    });
}


async addCoupon (){

  const modal = await this.modalController.create({
    component: VendorCouponAddPage,
    componentProps: {
       
    }
});
return await modal.present();


}


}
