import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VendorCouponListPage } from './vendor-coupon-list.page';

const routes: Routes = [
  {
    path: '',
    component: VendorCouponListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VendorCouponListPageRoutingModule {}
