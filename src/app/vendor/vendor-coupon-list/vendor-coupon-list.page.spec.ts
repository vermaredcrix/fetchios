import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VendorCouponListPage } from './vendor-coupon-list.page';

describe('VendorCouponListPage', () => {
  let component: VendorCouponListPage;
  let fixture: ComponentFixture<VendorCouponListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorCouponListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VendorCouponListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
