import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';
import { Settings } from './../../data/settings';
import { LoadingController} from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-order-payments',
  templateUrl: './order-payments.page.html',
  styleUrls: ['./order-payments.page.scss'],
})
export class OrderPaymentsPage implements OnInit {
  store_details : any;
  withdrawal_details:any;
  store_details__:any;
  vendorID: any;
  isVendorLogin: any;
  Mess = '';
  store_details_ = false;

  constructor(
    public settings: Settings,
    public loadingController: LoadingController, 
    public api: ApiService,
    private storage: Storage,
  ) {
    this.load_storage_data();
   }

  ngOnInit() {

     this.api.WCV2getAgentDeliveries('withdrawlist?vendor_id=' + this.vendorID).subscribe(res => {
      this.store_details_ = false;
       
          let ob = JSON.parse(JSON.stringify(res));
          this.withdrawal_details =  Object.values(ob['response']);
    
    
    
      }, err => {
    
          console.log(err);
    
      });

  }


  async load_storage_data(){

    this.vendorID = localStorage.getItem('vendorID');
    this.isVendorLogin = localStorage.getItem('isVendorLogin');

    this.storage.get('isVendorLogin').then((val) => {
        this.isVendorLogin = val;
    });

    this.storage.get('vendorID').then((val) => {
        this.vendorID = val;
    });


    // paymentlist
    // withdrawlist
      await this.api.WCV2getAgentDeliveries('paymentlist?vendor_id=' + this.vendorID).subscribe(res => {
         let ob = JSON.parse(JSON.stringify(res));
          this.store_details =  Object.values(ob['response']);

        this.store_details_ = true;


      }, err => {

          console.log(err);

      });

  }


  async Withdrawal(){
    this.store_details_ = false;



  }

  Tran(){
    this.store_details_ = true;
  }
  

}
